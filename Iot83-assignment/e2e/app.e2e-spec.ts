import { Iot83AssignmentPage } from './app.po';

describe('iot83-assignment App', function() {
  let page: Iot83AssignmentPage;

  beforeEach(() => {
    page = new Iot83AssignmentPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
